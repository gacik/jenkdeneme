package jenk;


import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class Frame extends JFrame{
	
	private JLabel lName;
	private JTextField tName;
	private JLabel output;
	
	public Frame(){
		this.setLayout(new FlowLayout());
		
		lName=new JLabel("Name:");
		tName=new JTextField(15);
		output=new JLabel();
		
		this.add(lName);
		this.add(tName);
		this.add(output);
		
		Handler handler = new Handler();
		tName.addActionListener(handler);
	}
	
	private class Handler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			output.setText("Welcome "+tName.getText());
			
		}
	}

}